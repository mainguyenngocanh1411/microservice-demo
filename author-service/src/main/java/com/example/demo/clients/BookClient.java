package com.example.demo.clients;

import com.example.demo.models.Book;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name="book-service")
public interface BookClient {

    @GetMapping("/author/{authorId}")
    List<Book> findByAuthor(@PathVariable("authorId") Long authorId);
}
