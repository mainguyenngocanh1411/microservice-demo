package com.example.demo.controllers;

import com.example.demo.clients.BookClient;
import com.example.demo.models.Author;
import com.example.demo.repositories.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.awt.print.Book;
import java.util.List;

@RestController
public class AuthorController {

    @Autowired
    AuthorRepository authorRepository;

    @Autowired
    BookClient bookClient;

    @GetMapping("/")
    public List<Author> findAll() {
        System.out.println("GET AUTHORS");
        return authorRepository.findAll();
    }

    @GetMapping("/{authorId}")
    public Author findById(@PathVariable("authorId") Long authorId){
        Author author = authorRepository.findById(authorId);
        author.setBooks(bookClient.findByAuthor(authorId));
        return author;
    }

}
