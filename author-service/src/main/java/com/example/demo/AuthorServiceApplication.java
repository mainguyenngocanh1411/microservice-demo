package com.example.demo;

import com.example.demo.models.Author;
import com.example.demo.repositories.AuthorRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class AuthorServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthorServiceApplication.class, args);
    }

    @Bean
    AuthorRepository repository() {
        AuthorRepository repository = new AuthorRepository();
        for (int i = 0; i < 10; i++) {
            Author author = new Author(i, "name " + i, 18);
            repository.add(author);
        }
        return repository;
    }

}
