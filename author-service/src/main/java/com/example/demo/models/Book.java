package com.example.demo.models;

public class Book {
    private int id;
    private String name;
    private int authorId;
    private String category;

    public Book(int id, String name, int authorId, String category) {
        this.id = id;
        this.name = name;
        this.authorId = authorId;
        this.category = category;
    }

    public Book(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
