package com.example.demo.repositories;

import com.example.demo.models.Author;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AuthorRepository {
    private List<Author> authors = new ArrayList<>();

    public List<Author> findAll() {
        return authors;
    }

    public Author add(Author author) {
        author.setId(authors.size() + 1);
        authors.add(author);
        return author;
    }
    
    public Author findById(Long authorId){
        Optional<Author> author = authors.stream().filter(a -> a.getId() == authorId).findFirst();
        return author.orElse(null);
    }
}
