package com.example.demo.controllers;

import com.example.demo.models.Book;
import com.example.demo.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BookController {

    @Autowired
    BookRepository bookRepository;

    @GetMapping("/")
    public List<Book> findAll() {
        System.out.println("GET BOOKS");
        return bookRepository.findAll();
    }

    @GetMapping("/author/{authorId}")
    public List<Book> findByAuthor(@PathVariable("authorId") Long authorId) {
        System.out.println("Book service - find list books from author id " + authorId);
        return bookRepository.findByAuthorId(authorId);
    }

}
