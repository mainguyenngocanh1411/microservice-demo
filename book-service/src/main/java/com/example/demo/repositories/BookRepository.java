package com.example.demo.repositories;

import com.example.demo.models.Book;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BookRepository {
    private List<Book> books = new ArrayList<>();

    public List<Book> findAll() {
        return books;
    }

    public Book add(Book book) {
        book.setId(books.size()+1);
        books.add(book);
        return book;
    }

    public List<Book> findByAuthorId(Long authorId){
        return books.stream().filter(book->book.getAuthorId()==authorId).collect(Collectors.toList());
    }
}
