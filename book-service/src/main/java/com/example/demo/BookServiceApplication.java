package com.example.demo;

import com.example.demo.models.Book;
import com.example.demo.repositories.BookRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
public class BookServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookServiceApplication.class, args);
    }
    @Bean
    BookRepository repository(){
        BookRepository repository = new BookRepository();
        for(int i = 0; i < 10; i++){
            Book book = new Book(i, "book " + i, i, "Comic");
            repository.add(book);
        }
        return repository;
    }

}
